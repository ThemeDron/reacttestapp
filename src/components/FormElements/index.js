import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './style.scss'


// Ошибки при валидации
const errorPatterns = {
    __empty__: 'Обязательное поле',
    __notNumber__: 'Введите цифры',
    __notPhone__: 'Пример: +7(903)123-45-67',
    __notEmail__: 'Недействительный емайл',
}


class FormElement extends Component {
    constructor(props) {
        super(props);
        
        this.validator     = this.validator.bind(this);
        this.handleChange  = this.handleChange.bind(this);
        
        this.statusFlag = false;
    }
    
    validator(prop) {
        this.props.validatorSet(prop);
    }
    
    handleChange(e) {
        this.props.onChange({
            value: e.target.value,
            id: this.props.id
        });
        
        this.statusFlag = true;
    }
    
    render() {
        let {value, 
             label, 
             type, 
             disabled, 
             validatorGet, 
             validator, 
             id, 
             name, 
             status} = this.props;
        
        let inputDOM = null;
        let labelDOM = <label htmlFor={id} className='formElement--label'>{label}</label>;
        
        let errorMSG   = null;
        let errorClass = '';
        
        // Проверим установку валидатора, если есть, то будем работать с ним
        if(this.props.validator) {
            this.validator({id, value, validator});
                
            if(this.statusFlag || status) {
                let msg = validatorGet[id].msg;
                
                // Отдадим в VALUE валидное значение
                value   = validatorGet[id].value;
                
                // Если есть текст ошибки, выведем ее и подсветим поле
                if(msg) {
                    errorClass = ' formElement--error';
                    errorMSG = (
                        <div className="formElement--errorMSG">
                            {errorPatterns[msg]}
                        </div>
                    )
                }
            } 
        }
        
        // Создадим input 
        const input = (
            <input 
                type={type} 
                value={value}
                id={id} 
                onChange={this.handleChange}
                disabled={disabled}
                className="formElement--input" />
        );     
        
        // Создадим textarea    
        const textarea = (
            <textarea 
                value={value}
                id={id} 
                onChange={this.handleChange} 
                className="formElement--textarea"
                rows="4"
                maxLength={250}></textarea>
        );
        
        // Создадим checkbox и radio    
        const checkbox = (
            <label htmlFor={id} className={'formElement--' + type}>
                <input 
                    type={type} 
                    name={name}
                    id={id} 
                    className={"formElement--" + type + "Input"} />
                <span className={'formElement--' + type + "Indicator"}></span>
                <span className={'formElement--' + type + "Description"}>{label}</span>
            </label>
        );
        
        // Проверим тип и выведем нужный элемент
        switch(type) {
            case 'text':
            case 'tel':
            case 'email':
            case 'password':
                inputDOM = input;
                break;
                
            case 'textarea':
                inputDOM = textarea;
                break;
                
            case 'checkbox':
            case 'radio':
                labelDOM = checkbox;
                break;
        }
        
        // Проверим поле на "пустоту" если не пустое повесим класс для плейсхолдера
        const isEmpty       = !value.length;
        const notEmptyClass = isEmpty ? '' : ' formElement--notEpmty';
        
        return (
            <div className={"formElement" + notEmptyClass + errorClass}>
                {inputDOM}
                {labelDOM}
                {errorMSG}
            </div>
        )
    }
}

export default FormElement