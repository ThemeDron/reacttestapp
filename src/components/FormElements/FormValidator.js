class FormValidator {
    errors = {}
    elems = {}
    
    // Создадим паттерны валидации
    patterns = {
        required: function(value) {
            if(value === '') 
                return {msg: '__empty__', value: value};
        },
        tel: function(value) {
            if(!value.match(/^[+7]*\([\d]{3}\)[\d]{3}-[\d]{2}-[\d]{2}$/)) 
                return {msg: '__notPhone__', value: value };
        },
        email: function(value) {
            if(!value.match(/^[-\._a-z0-9]+\@[-\._a-z0-9]+\.[a-z]{2,}$/)) 
                return {msg: '__notEmail__', value: value };
        },
        number: function(value) {
            let msg = false;
            if(value.match(/\D/)) {
                msg = '__notNumber__';
            }
            return {
                msg: msg,
                value: value.replace(/\D/, '')
            }
        },
    }
    
    hasValid({id, value, validator: patterns}) {
        this.elems[id] = {
            value: value, 
            msg: false,
            pattern: patterns
        }
        
        // Пробежим в цикле по настройкам валидатора
        for(let key in patterns) {
            const pattern = patterns[key];
            const result = this.patterns[pattern].call(this, value);
            
            if(result) {
                const {value: validValue, msg} = result;
                this.elems[id] = {
                    value: validValue, 
                    msg: msg,
                }
                
                if(msg) {
                    this.errors[id] = true
                }
                
                // Обработаем только текущий паттерн и вывалимся из цикла
                break; 
                
            } else {
                
                // Если паттерн вернул FALSE удалим значение из объекта ошибок
                delete this.errors[id];
            }
        }
    }

    isValid() {
        return this.elems
    }

    tryError() {
        for(let k in this.errors) {
            return true
        }
        return false
    }
}

export default FormValidator