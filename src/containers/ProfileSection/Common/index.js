import React, {Component} from 'react'
import PropTypes from 'prop-types'
import FormElement from '../../../components/FormElements'
import FormValidator from '../../../components/FormElements/FormValidator'

const Validator = new FormValidator();


class Common extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            companyName: '',
            companyNameT: 'Рога и копыта',
            companyNameF: 'Рога и копыта',
            companyAddress: '',
            companyPhone: '',
            companyEmail: '',
            companyTaxNumber: '',
            status: false
        }
        
        this.handleSubmit  = this.handleSubmit.bind(this);
        this.handleChange  = this.handleChange.bind(this);
        this.validatorSet  = this.validatorSet.bind(this);
    }

    static propTypes = {}
    
    validatorSet(e) {
        Validator.hasValid(e)
    }
    
    handleChange(e) {
        const {value, id} = e;
        this.setState({
            [id] : value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            status: true
        });
        if(!Validator.tryError()) {
            alert('Все норм, форма полетела')
        }
    }
    
    render(){
        return(
            <form className="container" onSubmit={this.handleSubmit}>
                <div className="row row--form w-800">
                    <FormElement 
                        id='companyName' 
                        type="text" 
                        label="Наименование организации" 
                        value={this.state.companyName} 
                        onChange={this.handleChange} 
                        status={this.state.status} 
                        validator={['required']}
                        validatorSet={this.validatorSet} 
                        validatorGet={Validator.isValid()} />
                </div>
                <div className="row row--form w-800">
                    <FormElement 
                        id='companyNameT' 
                        type="text" 
                        label="Наименование организации. Без валидации." 
                        value={this.state.companyNameT} 
                        onChange={this.handleChange} />
                </div>
                <div className="row row--form w-800">
                    <FormElement 
                        id='companyNameF' 
                        type="text" 
                        label="Наименование организации. Не активное" 
                        disabled={true} 
                        value={this.state.companyNameF} />
                </div> 
                <div className="row row--form w-800">
                    <FormElement 
                        id='companyAddress' 
                        type="textarea" 
                        label="Адрес (выберите из выпадающего списка)" 
                        value={this.state.companyAddress} 
                        onChange={this.handleChange} 
                        status={this.state.status} 
                        validator={['required']}
                        validatorSet={this.validatorSet} 
                        validatorGet={Validator.isValid()} />
                </div>
                <div className="row row--formEnd w-800">
                    <div className="formInline">
                        
                        <FormElement 
                            id='companyPhone' 
                            type="tel" 
                            label="Телефон" 
                            value={this.state.companyPhone} 
                            onChange={this.handleChange} 
                            status={this.state.status} 
                            validator={['required', 'tel']}
                            validatorSet={this.validatorSet} 
                            validatorGet={Validator.isValid()} />
                        
                        <FormElement 
                            id='companyEmail' 
                            type="email" 
                            label="E-mail" 
                            value={this.state.companyEmail} 
                            onChange={this.handleChange} 
                            status={this.state.status} 
                            validator={['required', 'email']}
                            validatorSet={this.validatorSet} 
                            validatorGet={Validator.isValid()} />
                        
                        <FormElement 
                            id='companyTaxNumber' 
                            type="text" 
                            label="ИНН" 
                            value={this.state.companyTaxNumber} 
                            onChange={this.handleChange} 
                            status={this.state.status} 
                            validator={['required', 'number']}
                            validatorSet={this.validatorSet} 
                            validatorGet={Validator.isValid()} />
            
                    </div>
                </div>
                <div className="row row--formEnd w-800">
                        <FormElement 
                            type="checkbox" 
                            id="checkbox" 
                            label="Checkbox без JS и без картинок"
                            value={this.state.companyTaxNumber} 
                            onChange={this.handleChange} />
                </div>
                <div className="row w-800 divider"></div>
                <div className="row row--formEnd w-800">
                        <FormElement 
                            type="radio" 
                            id="radioOne" 
                            name="radioGroup"
                            label="Radio без JS и без картинок"
                            value={this.state.companyTaxNumber} 
                            onChange={this.handleChange} />
            
                        <FormElement 
                            type="radio" 
                            id="radioTwo" 
                            name="radioGroup"
                            label="Radio без JS и без картинок"
                            value={this.state.companyTaxNumber} 
                            onChange={this.handleChange} />
                </div>
                <div className="row row--r w-800">
                    <input type="submit" value="Сохранить" className="btn btn--green"/>
                </div>
            </form>
        )
    }
}

export default Common