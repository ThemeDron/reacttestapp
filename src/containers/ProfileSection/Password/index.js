import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Password extends Component {

    static propTypes = {

    }

    render(){

        return(            
            <div className="container">
                <p className="hidden">
                    Изменение пароля. Нужно реализовать форму с инпутами и кнопкой и валидацией на фронте
                </p>
            
                <div className="row row--formEnd w-800">
                    <div className="formInline">
                        
                        <div className="formElement">
                            <input type="pass" id="oldPass" className="formElement--input"/>
                            <label htmlFor="oldPass" className="formElement--label">
                                Введите старый пароль
                            </label>
                        </div>
                        
                        <div className="formElement">
                            <input type="pass" id="newPass" className="formElement--input"/>
                            <label htmlFor="newPass" className="formElement--label">
                                Введите новый пароль
                            </label>
                            <div className="formElement--addon">
                                <span className="formElement--addonIcon">?</span>
                            </div>
                        </div>
                        
                        <div className="formElement">
                            <input type="text" id="tryNewPass" className="formElement--input"/>
                            <label htmlFor="tryNewPass" className="formElement--label">
                                Подтвердите новый пароль
                            </label>
                        </div>
                            
                    </div>
                </div>
                            
                <div className="row w-800 divider"></div>
            
                <div className="row row--l w-800">
                    <button className="btn btn--green">
                        Сохранить
                    </button>
                </div>
            </div>
        )
    }

}

export default Password