const webpack = require("webpack");
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const BUILD_NAME = './js/build.js';

const config = {
    entry: './src/app.js',
    output: {
        path: path.resolve('./www/static'),
        filename: BUILD_NAME,
        publicPath: "/"
    },
    devtool: 'source-map',
    watchOptions: { //задержка реакции чтобы исключить баги при задержке индексации файлов
        aggregateTimeout: 200
    },
    resolve: {
        modulesDirectories: ['node_modules', 'src'],// Директории в которых будут скать скрипты если не указан путь
        extansions: ['', '.js', '.jsx', '.scss', '.css']// разширения которые можно не дописывать при импорте
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel"
        }, {
            test: /\.(gif|png|ttf|svg|woff|eot)$/,
            loader: 'file-loader'
        }, {
            test: /\.(scss|css)$/,
            exclude: /node_modules/,
            loader: "style!css!resolve-url!sass-loader?sourceMap"
        }]
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/templates/dev_index.ejs',
            build: BUILD_NAME,
            inject: false,
            hash: true
        })
    ]
};

module.exports = config;